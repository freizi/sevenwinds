# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 20:46:54 2016

@author: freizi
"""

# Модуль распределения абилок персонажа

# Список характристик
char = {"Сила": 0, "Ловкость": 0, "Здоровье": 0, "Мудрость": 0}
# Доступное число скиллов
ABIL = 10
abil_char = 0
choice = None
while choice != "3":
    print("У вас доступно", ABIL-abil_char, "скиллов")
    print("Ваши характеристики:")
    for i in sorted(char.keys()):
        print (i, char[i])
    choice = input("1. Для увеличения характеристик\n2. Для уменьшения характеристик\n3. Для выхода\n")
# если выбор в меню 1, то повышаем характеристики  
    if choice == "1":
        abil_up = input("Какую характеристику улучшаем?\n").title()
        if abil_up in char:
            char[abil_up] += 1
            abil_char += 1
            print("Характеристика улучшена!")
        else:
            print("Такой характеристики нет!")
# если выбор в меню 2, то понижаем характеристики  
    elif choice == "2":
        abil_down = input("Какую характеристику уменьшаем?\n").title()
        if abil_down in char:
            char[abil_down] -= 1
            abil_char -= 1
            print("Характеристика снижена!")
# если выбор в меню 3, то выходим из программы        
    elif choice == "3":
        input('Жми "Enter" для выхода')   
# при не верном выборе в меню возвращаем в меню
    else:
        print("Неверный ввод!")
