# -*- coding: utf-8 -*-
"""
Created on Sun Aug  7 23:48:12 2016

@author: freizi
"""

#==============================================================================
# Добавить или искать слово (a/l)? a
# Введите слово: компьютер
# Введите определение: Машина, которая очень быстро считает
# Слово добавлено!
# Добавить или искать слово (a/l)? l
# Введите слово: компьютер
# Машина, которая очень быстро считает
# Добавить или искать слово (a/l)? l
# Введите слово: qwerty
# Это слово пока отсутствует в словаре.
#==============================================================================

#Slovar = {}
#Zapros = ""
#while not Zapros: 
#    Zapros = input("Добавить или искать слово (a/l)?")  
#while True:
#    if Zapros == "a":
#        Slovo = input("Введите слово:")
#        Opredelenie = input("Введите определение:")
#        Slovar[Slovo] = Opredelenie
#        print("Новое слово было добавлено в словарь!")
#        Zapros = input("Что дальше? Добавить или искать слово (a/l)?")
#        continue
#    elif Zapros == "l":
#        FindType = input("Искать отдельное слово или показать весь словарь? (g or m)")
#        if FindType == "g":
#            Slovo = input("Введите слово:")
#            if Slovo in Slovar:
#                print("Определение слова:", Slovar[Slovo])
#                Zapros = input("Что дальше? Добавить или искать слово (a/l)?")
#                continue
#            else:
#                print("Это слово пока отсутствует в словаре!")
#                Zapros = input("Что дальше? Добавить или искать слово (a/l)?")
#        elif FindType == "m":
#            for key in sorted(Slovar.keys()):
#                print (key, Slovar[key])
#        else:
#            print("Запрос некоректен")
#            
#    else:
#        Zapros = input("Введите корректный запрос... Добавить или искать слово (a/l)?")
#        continue


# версия 0.2 
#if Slovo in Slovar:
#   print("Определение слова:", Slovar[Slovo])
#   Zapros = ""
#   vozvrat()
#else:
#   print("Это слово пока отсутствует в словаре!")
#   Zapros = ""
#   vozvrat()
# Заменен более удачное проверкой



Slovar = {}

def vozvrat():
    global Zapros
    while Zapros != "a" and Zapros != "l":
        Zapros = input("Что дальше? Добавить или искать слово (a/l)?")
    deistvie(Zapros)

def dobavlenie():
    Slovo = input("Введите слово:")
    Opredelenie = input("Введите определение:")
    Slovar[Slovo] = Opredelenie
    print("Новое слово было добавлено в словарь!")
    return Slovar[Slovo], 
    
def deistvie(Change):
    global Zapros
    if Change == "a":
        dobavlenie()
        Zapros = ""
        vozvrat()
    elif Change == "l":
        FindType = input("Искать отдельное слово или показать весь словарь? (g or m)")
        if FindType == "g":
            Slovo = input("Введите слово:")
            print("Определение: " + Slovar.get(Slovo, "Это слово пока отсутствует в словаре!"))
            Zapros = ""
            vozvrat()
        elif FindType == "m":
            for key in sorted(Slovar.keys()):
                print (key, Slovar[key])
            Zapros = ""
            vozvrat()
        else:
            print("Запрос некоректен")
            Zapros = ""
            vozvrat()

Zapros = input("Добавить или искать слово (a/l)?")
while Zapros != "a" and Zapros != "l": 
    Zapros = input("Введите корректный запрос... Добавить или искать слово (a/l)?")
deistvie(Zapros)
