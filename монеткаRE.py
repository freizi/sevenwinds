# -*- coding: utf-8 -*-
"""
Created on Sun Aug 21 20:42:57 2016

@author: freizi
"""
#программа подбрасывает 100 монетку и выводит число "решек" и "орлов" из этих подбрасываний
from random import randint
print ('Программа подбрасывает 100 раз монетку\nи выводит число "решек" и "орлов" из этих подбрасываний')
choise = None
while not choise:
    podbros = int(input('Введите число бросков монеты:' ))
    orel = 0
    reshka = 0
    for i in range(podbros):
        if randint(1,2) == 1:
            orel += 1
        else:
            reshka +=1
    print ("орел выпал:", orel, "раз, ", "решка выпала:", reshka, "раз.")
    choise = input("Нажмите 'Enter' для нового броска, или введите любой символ для выхода\n")    
