# -*- coding: utf-8 -*-
"""
Created on Mon Aug  8 13:04:01 2016

@author: freizi
"""

class HotDog:
    def __init__(self):
        self.cooked_level = 0
        self.cooked_string = "Сырая"
        self.condiments = []
    def cook(self, time):
        self.cooked_level = self.cooked_level + time
        if self.cooked_level > 8:
            self.cooked_string = "Сгоревшая"
        elif self.cooked_level > 5:
            self.cooked_string = "Хорошо прожаренная"
        elif self.cooked_level > 3:
            self.cooked_string = "Средней прожарки"
        else:
            self.cooked_string = "Сырая"
myDog = HotDog()
myDog.cook(6)
print (myDog.cooked_level)
print (myDog.cooked_string)
print (myDog.condiments)

